# anki_cards_manager

## Description of the project
[Anki](https://apps.ankiweb.net/) is a program which helps remembering things.

I have been using it for years at my work. Almost daily, I am going through some cards of my study deck. Almost daily, I am feeding Anki with some new cards... as soon as I bump into everything which seems good to memorize. Thus, I have now a large amount of cards related to a wide range of topics.

In order to have my memorizing sessions only focused on the hot topics of the moment, I need to update the cards of my study deck on a regular basis.

I have just one deck dedicated to studying and it should only contains cards of interest.
Next to it, I have one deck for each technology I have been dealing with (technology is my own vocabulary… somewhat imprecise considering the usual definition). Each card is linked to a technology thanks to a corresponding tag.

Let’s say once a month, I update my study deck :
- cards which are not needed in a short or medium term are sent to their technology deck.
- cards related to the technologies which will be good to know soon are moved to the study deck.

Originally, I was going through this process manually and it was time consuming. This tool is a dashboard which helps me to sort my cards in less than two minutes. It shows all the technologies (based on the tags of your cards) on your web browser and you can just select the ones you want to focus on.


In addition, my cards also contains some special tags which are not technology related but giving an information on the type of content. For exemple :
- a card tagged with "Concept" explain a fondamental concept on a technology
- a card tagged with "Procedure" will give practical rules or examples related to the use of the technology
- a card tagged with "CultureGénérale" relates more to general knowledge on the technology

Using those tags in your decks, the dashboard offer you to select the type of cards you want to have in you study deck for each technology.

An additional option allows to select only the red flagged anki cards for each technology. That’s the cards I am considering of high importance.

## How can you use/try this tool ?
This program was developped on a Linux OS and I have no idea at the moment if it works properly on another OS (checking that is not in my pipe at the moment but if one person ever contact me for that, I am open to discuss a solution).
[Docker](https://docs.docker.com/engine/install/ubuntu/) (version 20.10.1) and [Docker Compose](https://docs.docker.com/compose/install/) (version 1.25.0) also need to be installed.

Clone the project on your computer.
Install Anki if needed : follow the istructions of the [official website](https://apps.ankiweb.net/) OR install from the official Ubuntu Software Catalogue
Open your anki app.
If your cards are not following the tags and decks structure mentioned hereabove, I am still offering you a first shot if you are motivated enough to follow those steps :
- Create a fresh Anky profile in 5 clicks or 20 seconds (it is not necesssary but recommanded for a better understanding of this small tutorial): go to the main page on your anki app, click **File**, click **Switch Profile**, click **Add**, enter a name, Click **OK**, select your new Profil name, click **Open**.
- Import the 4 decks named **Football.apkg**, **Journalisme.apkg**, **Musique.apkg**, **Rugby.apkg** following those steps for each deck : click **File**, click **Import**, find the following directory **directory_where_you_cloned_the_app/anki_cards_manager/mydashboard/anky_cards_manager/ankimanager/Data**, select one the corresponding .apkg file, click **Open**. Your Anki app should look like this :

![](/Pictures/anki_decks.png)

- Add the Anki connect add-on to your anki app (click **Tools**, click : **Add-ons**, click **Get Add-ons...** and enter the folowing code 2055492159 coming from the [AnkiConnect web page](https://ankiweb.net/shared/info/2055492159), then click **OK**).
- Open the project folder and open the terminal then run the command **docker-compose up**. Open your browser and connect to the following adress: **http://0.0.0.0:8000/**. The following dashboard should appear :

![](/Pictures/dashboard.png)

Checking the box as above, and submiting your changes, a new study deck named AAA_Study_Deck appears in Anky and might contains the folowwing cards :

![](/Pictures/anki_study_deck.png)

Please, enjoy !

## Technologies
- Python 3.6.9
- Django 3.1.3
- Bootstrap
- Docker 20.10.1
- AnkiConnect

## Contribute to the project
You are welcome !

## Authors
Our code squad : Benoit

## Licensing
Free for everybody except those who have a number with more than six zero on their bank account
