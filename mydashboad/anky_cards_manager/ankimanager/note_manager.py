
"""
This library implement several methods to interact with Anki notes,
cards, decks... via the AnkiConnect API.
"""


from .util import invoke

def importPackage(path):
    """
    path (string) where the package is stored
    """
    invoke('importPackage', path=path)


def loadProfile(name):
    """ Load the Profile "name". Anki has to be open. """
    invoke('loadProfile', name=name)

def deleteDecks(decks):
    """
    decks is a list of strings (names of the decks to delete)
    Delete the decks given as a parameter. the cards are deleted as well.
    """
    invoke('deleteDecks', decks=decks, cardsToo=True)

def deleteNotes(notes_id):
    """
    Delete a list of notes.
    """
    invoke('deleteNotes', notes=notes_id)

def synchronizeAnki():
    """Synchronizes the local Anki collections with AnkiWeb."""
    return invoke('sync')

def usedDecksList():
    """Return a list containing the list of decks"""
    return invoke('deckNames')

def createDeck(deck_name):
    """
    Create a new empty deck
    deck_name is a string
    """
    return invoke('createDeck', deck=deck_name)

def createNote(deck_name, front_content, back_content, tag_list, model_name = "Basic"):
    """
    Create a new Note
    deckName (string)
    tag_list (string) is a list of tag
    front_content (string)
    back content (string)
    """
    note = {
    "deckName": deck_name,
    "modelName": model_name,
    "fields": {
        "Front": front_content,
        "Back": back_content,
    },
    "tags": tag_list,
    }
    return invoke('addNote', note=note)

def findNotesUsingFilter(filter):
    """
    Return a list containing all the Notes IDs matching filter
    filter is a string containing the parameters of a query
    """
    return invoke('findNotes', query=filter)


def findCardsUsingFilter(filter):
    """
    Return a list containing all the Cards IDs matching filter
    filter is a string containing the parameters of a query
    """
    return invoke('findCards', query=filter)


def extractTagsFromNotes(notes_id):
    """
    Return a set tags values (string)
    notes_id is a list of Notes ID
    """
    tag_set = set()
    for note in invoke('notesInfo', notes=notes_id):
        for tag in note['tags']:
            tag_set.add(tag)
    return tag_set


def extractDecksOfCards(cards_ids):
    """
    Return a set of decks names (strings)
    cards_ids is a liste of Cards ID
    """
    return set(invoke('getDecks', cards=cards_ids).keys())


def usedTagsList():
    """Return a set containing the complete list of used tags"""
    special_tags = ["Concept", "Procedure", "CultureGénérale", "TableauBlanc"]
    return {elt for elt in extractTagsFromNotes(findNotesUsingFilter(''))
                if elt not in special_tags}


def cardsmoveToDeck(cards_ids, new_deck):
    """
    Moves cards with given IDs to a targeted deck, creating the deck if it doesn't
    exist yet.
    cards_ids is a list
    new_deck is the targeted deck (string)
    """
    assert new_deck in usedDecksList(), "new_deck doesn't exist"
    return invoke('changeDeck', cards=cards_ids, deck=new_deck)


def cardsWithTagsmoveToDeck(tag_list, new_deck, red=False):
    """
    Moves cards containing all the tags of tag_list to a targeted deck, creating
    the deck if it doesn't exist yet.
    tag_list is the list of tags (strings)
    new_deck is the targeted deck (string)
    red moves only red flagged notes
    """
    assert new_deck in usedDecksList(), "new_deck doesn't exist"
    filter = "tag:" + " tag:".join(tag_list)
    if red:
        filter = filter + " flag:1"
    #print('CARDS WITH', filter, 'MOVE TO DECK', new_deck)
    cardsmoveToDeck(findCardsUsingFilter(filter), new_deck)
