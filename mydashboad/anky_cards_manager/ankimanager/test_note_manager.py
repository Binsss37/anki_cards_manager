"""
Test library for note_manager
"""

import unittest
from pathlib import Path
from . import util
from . import note_manager



print('Check that Anki is open on the main page and not busy (like adding/modifying \
notes). Then, type OK and press Enter :')
answer = input()
while answer != 'OK' :
    print('Type OK and press Enter :')
    answer = input()

#@unittest.skip
class AnkiNoteManagerTest(unittest.TestCase):
    """
    Tests of the connexion between Anki and the Django App
    """

    def setUp(self):
        test_account = 'ANKI_CARDS_MANAGER_TEST'
        util.invoke('loadProfile', name=test_account)

        self.test_deck_list = ['Journalisme', 'Musique', 'Football', 'Rugby']

        decks_to_clear = util.invoke('deckNames')
        util.invoke('deleteDecks', decks=decks_to_clear, cardsToo=True)

        for deckname in self.test_deck_list:
            data_dir = Path(__file__).parent.joinpath('Data/{}.apkg'.format(deckname)).resolve()
            note_manager.importPackage(path = str(data_dir))


    def tearDown(self):
        util.invoke('deleteDecks', decks=self.test_deck_list, cardsToo=True)

        #user_account = 'User 1'
        #util.invoke('loadProfile', name=user_account)

    def test_usedDecksList(self):
        decks = note_manager.usedDecksList()
        expected_decks = ['Journalisme', 'Musique', 'Football', 'Rugby', "Default"]
        self.assertCountEqual(expected_decks, decks,
        	"usedTagsList() doesn't return the right list")
        for elt in expected_decks:
            self.assertIn(elt, decks,
            	"usedDecksList doesn't return the following deck : {}".format(elt))

    def test_createDeck(self):
        note_manager.createDeck('Added_deck')
        self.assertIn('Added_deck', note_manager.usedDecksList(),
                      'Added_deck has not been added to the decks')

    def test_createNote(self):
        note_manager.createNote(
            deck_name="Football",
            model_name="Basic",
            front_content="Quel joueur a joué à Naple et est devenu champion du\
                          monde avec l'Argentine ?",
            back_content = "Diego Maradonna",
            tag_list = ["Football", "CultureGénérale"],
        )
        self.assertEqual(len(note_manager.findNotesUsingFilter("deck:Football")), 5,
        	"the deck Football should contain 5 notes after adding one (6 cards)")

    def test_deleteNotes(self):
        notes = note_manager.findNotesUsingFilter("deck:Football")
        note_manager.deleteNotes(notes)
        self.assertEqual(len(note_manager.findNotesUsingFilter("deck:Football")), 0,
        "The Football deck should be empty afer deleting all the notes with deleteNotes()")

    def test_usedTagsList(self):
        expected_list = [
        'Football',
        'Rugby',
        'Journalisme',
        'Sport',
        'Musique',
        ]
        self.assertCountEqual(expected_list, note_manager.usedTagsList(),
        	"usedTagsList() doesn't return the right list")
        for elt in expected_list:
            self.assertIn(elt, note_manager.usedTagsList(),
            	"usedTagsList doesn't return the following tag : {}".format(elt))



    def test_cardsmoveToDeck(self):
        filter = 'tag:Sport'
        new_deck = "AAA_Study_Deck"
        note_manager.createDeck(new_deck)
        cards_ids = note_manager.findCardsUsingFilter(filter=filter)
        note_manager.cardsmoveToDeck(cards_ids, new_deck)
        final_decks = note_manager.extractDecksOfCards(cards_ids)
        self.assertEqual(len(final_decks), 1,
        	"all cards should ends up in only one deck which is :{}".format(new_deck))
        self.assertEqual(new_deck, final_decks.pop(),
        	"cards haven't been able to move in the right deck")


    def test_cardsWithTagsmoveToDeck(self):
        tag_list = ['Journalisme', 'CultureGénérale']
        new_deck = "AAA_Study_Deck"
        note_manager.createDeck(new_deck)
        filter = "tag:" + " tag:".join(tag_list)
        cards_ids = note_manager.findCardsUsingFilter(filter=filter)
        note_manager.cardsWithTagsmoveToDeck(tag_list, new_deck)
        final_decks = note_manager.extractDecksOfCards(cards_ids)
        self.assertEqual(len(final_decks), 1,
        	"all cards should ends up in only one deck which is :{}".format(new_deck))
        self.assertEqual(new_deck, final_decks.pop(),
        	"cards haven't been able to move in the right deck")




if __name__ == '__main__':
    unittest.main()
