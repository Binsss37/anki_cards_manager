from django.db import models
from .ankimanager import note_manager

# Create your models here.

class Tag(models.Model):
    """
    Define the tag database parameters.
    """
    name = models.CharField(max_length=5000, unique=True, db_column='tag')
    selected = models.BooleanField(default=False)
    concept_selected = models.BooleanField(default=False)
    procedure_selected = models.BooleanField(default=False)
    culturegenerale_selected = models.BooleanField(default=False)
    red_selected = models.BooleanField(default=False)

    special_tags = ["Concept", "Procedure", "CultureGénérale", "TableauBlanc"]

    def __str__(self):
        return self.name

    class Meta:
        ordering= ['name']

    @classmethod
    def update_tags_info(cls):
        """
        synchronize the tags recorded in the database with the tags in use in Anki
        """
        special_tags = ["Concept", "Procedure", "CultureGénérale", "TableauBlanc"]
        for name in special_tags:
            cls.objects.filter(name=name).delete()
        db_tag_list = {tag.name for tag in cls.objects.all() if tag.name not in special_tags}
        anki_tag_list = {elt for elt in note_manager.usedTagsList()}
        new_db_tags = anki_tag_list - db_tag_list
        for elt in new_db_tags:
            cls.objects.create(
                name=elt,
                selected=False,
                concept_selected=False,
                procedure_selected=False,
                culturegenerale_selected=False,
                red_selected=False)
        old_db_tags = db_tag_list - anki_tag_list
        for elt in old_db_tags:
            cls.objects.filter(name=elt).delete()
