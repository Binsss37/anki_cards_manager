"""
Anki cards manager URL Configuration
"""

from django.urls import path

from . import views

app_name = 'anky_cards_manager'

urlpatterns = [
    path('', views.tags_manager_view, name='tags_manager'),
]
