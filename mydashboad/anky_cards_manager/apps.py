from django.apps import AppConfig


class AnkyCardsManagerConfig(AppConfig):
    name = 'anky_cards_manager'
