"""
Test to the anky_cards_manager application
"""
import os
from unittest import skip
from time import sleep
from pathlib import Path

from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains

from .models import Tag
from .ankimanager import note_manager, util
from .views import empty_study_deck


# Create your tests here.

# Dashboard page

#@skip
class anky_cards_managerAppTest(TestCase):
    """
    Tests of the connexion between Anki and the Django App
    """

    def setUp(self):
        #################### ANKI SETUP
        test_account = 'ANKI_CARDS_MANAGER_TEST'
        note_manager.loadProfile(name=test_account)

        self.test_deck_list = ['Journalisme', 'Musique', 'Football', 'Rugby']

        decks_to_clear = util.invoke('deckNames')
        note_manager.deleteDecks(decks=decks_to_clear)

        for deckname in self.test_deck_list:
            data_dir = Path(__file__).parent\
                                     .joinpath('ankimanager/Data/{}.apkg'.format(deckname))\
                                     .resolve()
            util.invoke('importPackage', path = str(data_dir))

        #################### DATABASE SETUP
        test_list = [
            ['Tag1', True, True, True, True, True],
            ['Tag2', False, False, False, False, False],
            ['Tag3', False, True, False, True, False],
            ['Tag4', True, False, False, False, False],
            ['Tag5', True, False, False, False, False],
            ['Tag6', True, False, False, False, False],
        ]
        for elt in test_list:
            Tag.objects.create(
                name=elt[0],
                selected=elt[1],
                concept_selected=elt[2],
                procedure_selected=elt[3],
                culturegenerale_selected=elt[4],
                red_selected=elt[5])

    def tearDown(self):
        note_manager.deleteDecks(decks=self.test_deck_list)

    def get_db_tags_names(self):
        """"
        Returns a set containing all tag names registered in the Tag model.
        """
        model_tag_list = {tag.name for tag in Tag.objects.all()}
        return model_tag_list

    def anki_and_db_tags_comparison(self,message):
        """
        Compare that the tags in Anki and in the db are the same.
        """
        anki_tag_list = note_manager.usedTagsList()
        db_tag_list = self.get_db_tags_names()
        self.assertEqual(anki_tag_list, db_tag_list, message)

    def test_tags_manager(self):
        #TODO: test the post request
        """
        Test que l'url du tags_manager donne :
            - le bon template
            - le bon contexte
            - renvoie un status HTTP 200
        """
        expected_context_tags = ['Football', 'Sport', 'Journalisme', 'Rugby', 'Musique']
        response = self.client.get(reverse('anky_cards_manager:tags_manager'))
        context_tags = response.context['tags']
        self.assertTemplateUsed(response, 'anky_cards_manager/tags_manager.html',
        	"The wrong template is used. 'anky_cards_manager/tags_manager.html' is expected")
        self.assertEqual(len(context_tags), len(expected_context_tags),
        	"the context should contains {} tags elements".format(len(expected_context_tags)))
        for context_elt in context_tags:
            self.assertIn(context_elt.name, expected_context_tags,
            	"{} should be {}".format(context_elt, expected_context_tags))
            expected_context_tags.remove(context_elt.name)
        self.assertEqual(response.status_code, 200)


    def test_update_db_tags(self):
        """
        test that the list of tags recorded in the db is updated when the tag manager
         web page is requested
        """
        self.client.get(reverse('anky_cards_manager:tags_manager'))
        self.anki_and_db_tags_comparison(message="the database should contain exactly \
        the same tags as anki when loading the tag manager web page")


#@skip
class anky_cards_manager_WebPageTest(StaticLiveServerTestCase):
    """
    Tests for the front part of the anky_cards_manager Django app
    """
    def setUp(self):
        #################### ANKI SETUP
        test_account = 'ANKI_CARDS_MANAGER_TEST'
        note_manager.loadProfile(name=test_account)

        self.test_deck_list = ['Journalisme', 'Musique', 'Football', 'Rugby']

        decks_to_clear = util.invoke('deckNames')
        note_manager.deleteDecks(decks=decks_to_clear)

        for deckname in self.test_deck_list:
            data_dir = Path(__file__).parent\
                                     .joinpath('ankimanager/Data/{}.apkg'.format(deckname))\
                                     .resolve()
            util.invoke('importPackage', path = str(data_dir))

        #################### DATABASE SETUP
        test_list = [
            ['Tag1', True, True, True, True, True],
            ['Tag2', False, False, False, False, False],
            ['Tag3', False, True, False, True, False],
            ['Tag4', True, False, False, False, False],
            ['Tag5', True, False, False, False, False],
            ['Tag6', True, False, False, False, False],
        ]
        for elt in test_list:
            Tag.objects.create(
                name=elt[0],
                selected=elt[1],
                concept_selected=elt[2],
                procedure_selected=elt[3],
                culturegenerale_selected=elt[4],
                red_selected=elt[5])

    def tearDown(self):
        note_manager.deleteDecks(decks=self.test_deck_list)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def get_tag_info_from_form_row(self, row):
        '''
        Need a row of the tag manager table as an input.
        Returns a dictionary containing the corresponding tag name and the status
        for each checkbox.
        '''
        row_cells = row.find_elements_by_tag_name("th") + row.find_elements_by_tag_name("td")
        tag_info = {}
        tag_info["name"] = row_cells[0].text
        for cell in row_cells[1:]:
            elt = cell.find_element_by_tag_name("input")
            if elt.is_selected() :
                tag_info[elt.get_attribute("id").split('.')[-1]] = True
            else :
                tag_info[elt.get_attribute("id").split('.')[-1]] = False
        return tag_info

    def get_tags_info_from_form(self):
        """"
        Returns a dictionary containing, for every line of the tag manager table,
        the corresponding tag name as a key and another dictionary as a value.
        This sub-dictionary contains the corresponding tag name and the values
        of each parameter ("concept", "procedure", "red"...) as shown by the Web
        page.
        """
        tag_list = {}
        rows = self.selenium.find_element_by_id("tag_manager_table_body")\
            .find_elements_by_tag_name("tr")
        for row in rows :
            new_tag = self.get_tag_info_from_form_row(row)
            tag_list.update({new_tag['name']: new_tag})
        return tag_list

    def get_db_tag_info(self, tag_instance):
        '''
        Need a Tag instance as an input.
        Returns a dictionary containing the corresponding tag name and the status
        for each parameter ("concept", "procedure", "red"...).
        '''
        tag_info = {}
        tag_info["name"] = tag_instance.name
        tag_info["selected"] = tag_instance.selected
        tag_info["concept_selected"] = tag_instance.concept_selected
        tag_info["procedure_selected"] = tag_instance.procedure_selected
        tag_info["culturegenerale_selected"] = tag_instance.culturegenerale_selected
        tag_info["red_selected"] = tag_instance.red_selected
        return tag_info

    def get_db_tags_info(self):
        """"
        Returns a dictionary containing, for every instance of the Tag model,
        the corresponding tag name as a key and another dictionary as a value.
        This sub-dictionary contains the corresponding tag name and the values
        of each parameter ("concept", "procedure", "red"...)
        """
        tag_list = {}
        for tag in Tag.objects.all():
            new_tag = self.get_db_tag_info(tag)
            tag_list.update({new_tag['name']: new_tag})
        return tag_list

    def form_and_db_tags_info_comparison(self, form_tag_list, db_tag_list):
        """
        Compare that the tags in the db and in the form are the same.
        """
        self.assertEqual(len(form_tag_list.keys()), len(db_tag_list.keys()),
                         "the number of tags in the dashboard should be the \
                         same as the number of tags in the sqlite database")
        self.assertEqual(form_tag_list.keys(), db_tag_list.keys(),
                         "the tags in the dashboard should be the same as the \
                         tags in the sqlite database")

        for tag in form_tag_list.keys():
            for tag_property in form_tag_list[tag].keys():
                self.assertEqual(
                	form_tag_list[tag][tag_property],
                	db_tag_list[tag][tag_property],
                	"the tag {} should have the same property {} in the dashboard \
                	and in the database.\nIn the dashboard, the value is : \
                	{}\nIn the database, the value is {}".format(
                		tag,
                		tag_property,
                		form_tag_list[tag][tag_property],
                		db_tag_list[tag][tag_property]
                		)
                	)

    #@skip("not needed during development")
    def test_tags_form_vs_db(self):
        """
        Check that the tags info shown on the web page are the same as the tags
        info recorded in the Database.
        """
        self.selenium.get('%s%s' % (self.live_server_url, ''))
        form_tag_list = self.get_tags_info_from_form()
        db_tag_list = self.get_db_tags_info()
        self.form_and_db_tags_info_comparison(form_tag_list, db_tag_list)


    #@skip("not needed during development")
    def test_tags_manager_page(self):
        """
        Check that the tags names shown on the web page are the same as the tags
         names recorded in the Database.
        """
        self.selenium.get('%s%s' % (self.live_server_url, ''))
        tags_in_form = self.get_tags_info_from_form().keys()
        tags_registered_in_db = {tag.name for tag in Tag.objects.all()}
        self.assertEqual(len(tags_in_form), len(tags_registered_in_db),
                         "the number of lines in the tag manager form and the number\
                          of tag registered in the Tag db should be equal")
        self.assertEqual(len(tags_in_form - tags_registered_in_db), 0,
                         "row_names and tags_registered_in_db should the same elements")

    #@skip("expected to fail at the moment (UNDER DEVELOMENT)")
    def test_submit_button_update_db(self):
        """"
        Check a box in the web page and test if the db is updated as expected
        when the form is submitted.
        """

        self.selenium.get('%s%s' % (self.live_server_url, ''))

        initial_form_tag_list = self.get_tags_info_from_form()
        initial_db_tag_list = self.get_db_tags_info()
        self.form_and_db_tags_info_comparison(initial_form_tag_list, initial_db_tag_list)

        box_to_check = self.selenium.find_element_by_id("Sport.culturegenerale_selected")
        actions = ActionChains(self.selenium).move_to_element(box_to_check).click()
        actions.perform()
        sleep(1)

        submit_button = self.selenium.find_element_by_id("modal_tag_manager_submit")
        actions = ActionChains(self.selenium).move_to_element(submit_button).click()
        actions.perform()
        sleep(1)

        submit_button = self.selenium.find_element_by_id("tag_manager_submit")
        actions = ActionChains(self.selenium).move_to_element(submit_button).click()
        actions.perform()
        sleep(1)

        final_form_tag_list = self.get_tags_info_from_form()
        final_db_tag_list = self.get_db_tags_info()

        self.assertNotEqual(initial_form_tag_list['Sport']['culturegenerale_selected'],
                            final_form_tag_list['Sport']['culturegenerale_selected'])
        self.form_and_db_tags_info_comparison(final_form_tag_list, final_db_tag_list)

    #@skip("Not needed during development")
    def test_empty_study_deck(self):
        """
        Check that an alert message is returned when a note of the study deck \
        has no tag corresponding to a deck name
        """
        study_deck = "AAA_Study_Deck"
        note_manager.createDeck(study_deck)

        note_manager.createNote(
            deck_name=study_deck,
            model_name="Basic",
            front_content="Quel joueur a joué à Naple et est deveu champion du"
                          " monde avec l'Argentine ?",
            back_content = "Diego Maradonna",
            tag_list = ["Football", "CultureGénérale"],
        )

        note_manager.createNote(
            deck_name=study_deck,
            model_name="Basic",
            front_content="Quel célèbre patineur francais est maintenant devenu"
                          " journaliste ?",
            back_content="Philippe Candeloro",
            tag_list=["patinage", "CultureGénérale"],
        )

        alert = empty_study_deck()
        expected_alert = "We were not able to find an host deck for all the notes" \
                         " of AAA_Study_Deck. Every note should contain at least one" \
                         " tag corresponding to a deck name"
        self.assertEqual(
            alert,
            expected_alert,
            "The alert message is not the right one")
        self.assertEqual(
            len(note_manager.findCardsUsingFilter("deck:{}".format(study_deck))), 1,
            """The study deck should contain 1 remaning note : "Quel célèbre patineur
             francais est maintenant devenu journaliste ?" """)

    #@skip("Not needed during development")
    def test_empty_study_deck_v2(self):
        """
        Check that no alert message is returned all study deck notes \
        have at least one tag corresponding to a deck name
        """
        study_deck = "AAA_Study_Deck"
        note_manager.createDeck(study_deck)

        note_manager.createNote(
            deck_name=study_deck,
            model_name="Basic",
            front_content="Quel joueur a joué à Naple et est deveu champion du"
                          " monde avec l'Argentine ?",
            back_content = "Diego Maradonna",
            tag_list = ["Football", "CultureGénérale"],
        )

        alert = empty_study_deck()
        expected_alert = ""
        self.assertEqual(
            alert,
            expected_alert,
            "The alert message is not the right one")
        self.assertEqual(len(note_manager.findCardsUsingFilter(
            "deck:{}".format(study_deck))),
            0,
            """The study deck should contain 1 remaning note : "Quel célèbre patineur
             francais est maintenant devenu journaliste ?" """)


    #@skip("Not needed during development")
    def test_synchronize_cards(self):
        ''''
        Check that the the cards synchronize well after the tag manager form submission
        '''
        study_deck = "AAA_Study_Deck"
        note_manager.createDeck(study_deck)

        self.selenium.get('%s%s' % (self.live_server_url, ''))

        box_to_check = self.selenium.find_element_by_id("Journalisme.culturegenerale_selected")
        actions = ActionChains(self.selenium).move_to_element(box_to_check).click()
        actions.perform()
        sleep(1)
        submit_button = self.selenium.find_element_by_id("modal_tag_manager_submit")
        actions = ActionChains(self.selenium).move_to_element(submit_button).click()
        actions.perform()
        sleep(1)
        submit_button = self.selenium.find_element_by_id("tag_manager_submit")
        actions = ActionChains(self.selenium).move_to_element(submit_button).click()
        actions.perform()
        sleep(1)
        self.assertEqual(
            len(note_manager.findCardsUsingFilter("deck:{}".format(study_deck))),
            4,
            """The study deck should contain 4 notes when selecting 'Culture Générale'
             for the Journalisme tag""")

        box_to_check = self.selenium.find_element_by_id("Journalisme.concept_selected")
        actions = ActionChains(self.selenium).move_to_element(box_to_check).click()
        actions.perform()
        sleep(1)
        submit_button = self.selenium.find_element_by_id("modal_tag_manager_submit")
        actions = ActionChains(self.selenium).move_to_element(submit_button).click()
        actions.perform()
        sleep(1)
        submit_button = self.selenium.find_element_by_id("tag_manager_submit")
        actions = ActionChains(self.selenium).move_to_element(submit_button).click()
        actions.perform()
        sleep(1)
        alert_message = self.selenium.find_element_by_id("tag_manager_table_form")\
                                     .find_element_by_tag_name("div").text
        self.assertEqual(alert_message, "")
        sleep(2)
        self.assertEqual(
            len(note_manager.findCardsUsingFilter("deck:{}".format(study_deck))),
            6,
        	"the study deck should contain 6 notes when selecting 'Culture Générale'"
            " and 'Concept' for the Journalisme tag")

        box_to_check = self.selenium.find_element_by_id("Journalisme.culturegenerale_selected")
        actions = ActionChains(self.selenium).move_to_element(box_to_check).click()
        actions.perform()
        sleep(1)
        submit_button = self.selenium.find_element_by_id("modal_tag_manager_submit")
        actions = ActionChains(self.selenium).move_to_element(submit_button).click()
        actions.perform()
        sleep(1)
        submit_button = self.selenium.find_element_by_id("tag_manager_submit")
        actions = ActionChains(self.selenium).move_to_element(submit_button).click()
        actions.perform()
        sleep(1)
        alert_message = self.selenium.find_element_by_id("tag_manager_table_form")\
            .find_element_by_tag_name("div").text
        print(alert_message)
        self.assertEqual(alert_message, "")
        self.assertEqual(
            len(note_manager.findCardsUsingFilter("deck:{}".format(study_deck))),
            2,
            """the study deck Football should contain 6 notes when selection 'Concept'
             for the Journalisme tag""")



#@skip
class anky_cards_manager_FormTest(StaticLiveServerTestCase):
    """
    Tests for the front part of the anky_cards_manager Django app
    """
    def setUp(self):
        #################### ANKI SETUP
        test_account = 'ANKI_CARDS_MANAGER_TEST'
        note_manager.loadProfile(name=test_account)

        self.test_deck_list =[(elt.rstrip(".apkg"), "ankimanager/Data_Big/" + elt)
                              for elt in os.listdir('anky_cards_manager/ankimanager/Data_Big')
                              if elt.endswith('.apkg')]

        decks_to_clear = util.invoke('deckNames')
        note_manager.deleteDecks(decks=decks_to_clear)

        for deckname, deckpath in self.test_deck_list:
            data_dir = Path(__file__).parent.joinpath(deckpath.format(deckname)).resolve()
            util.invoke('importPackage', path = str(data_dir))

        # As empty decks are not imported, they are added hereunder
        for elt in ['CSS-selectors', 'API_REST', 'Test', 'SGBD-SQLite']:
            note_manager.createDeck(elt)


    def tearDown(self):
        note_manager.deleteDecks(decks=self.test_deck_list)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_big_data(self):
        '''
        '''
        deck_list = note_manager.usedDecksList()
        deck_list.remove("Default")
        packages_list = os.listdir('anky_cards_manager/ankimanager/Data_Big')
        self.assertEqual(
            len(deck_list),
            len(packages_list),
            "some packages have not been uploaded"
        )
        for elt in deck_list :
            self.assertIn(
                elt + ".apkg",
                packages_list,
                "{} is an anki deck but and not corresponding to a any package"
            )
            packages_list.remove(elt + ".apkg")
        self.assertEqual(
            len(packages_list),
            0,
            "The following packages have not been imported in Anki : {}".format(packages_list)
        )
