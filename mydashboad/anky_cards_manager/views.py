from django.shortcuts import render
from .models import Tag
from .ankimanager import note_manager, util


# Create your views here.

def empty_study_deck():
    study_deck = "AAA_Study_Deck"
    study_deck_notes = note_manager.findNotesUsingFilter('deck:{}'.format(study_deck))
    study_deck_tags = [elt for elt in note_manager.extractTagsFromNotes(study_deck_notes) if
                        elt not in Tag.special_tags]
    decks = note_manager.usedDecksList()
    for tag in study_deck_tags:
        if tag not in decks:
            continue
        else:
            note_manager.cardsWithTagsmoveToDeck(tag_list=[tag], new_deck=tag)
    alert = ""
    remaining_study_deck_notes = note_manager.findNotesUsingFilter('deck:{}'.format(study_deck))
    if len(remaining_study_deck_notes) != 0:
        alert = "We were not able to find an host deck for all the notes of AAA_Study_Deck. " \
                "Every note should contain at least one tag corresponding to a deck name"
    return alert

def synchronize_cards():

    study_deck = "AAA_Study_Deck"
    decks = note_manager.usedDecksList()
    if study_deck not in decks:
        note_manager.createDeck(study_deck)

    alert = empty_study_deck()

    i = 0
    for elt in Tag.objects.all():
        i += 1
        if elt.selected == False\
        and elt.concept_selected == False\
        and elt.procedure_selected == False\
        and elt.culturegenerale_selected == False\
        and elt.red_selected == False :
            pass
        else:
            tag_list = [elt.name]
            if elt.selected:
                note_manager.cardsWithTagsmoveToDeck(tag_list=tag_list,
                                                     new_deck=study_deck,
                                                     red=elt.red_selected)
            else :
                if elt.concept_selected:
                    note_manager.cardsWithTagsmoveToDeck(
                        tag_list=tag_list + ["Concept"],
                        new_deck=study_deck,
                        red=elt.red_selected
                    )
                if elt.procedure_selected:
                    note_manager.cardsWithTagsmoveToDeck(
                        tag_list=tag_list + ["Procedure"],
                        new_deck=study_deck,
                        red=elt.red_selected
                    )
                if elt.culturegenerale_selected:
                    note_manager.cardsWithTagsmoveToDeck(
                        tag_list=tag_list + ["CultureGénérale"],
                        new_deck=study_deck,
                        red=elt.red_selected
                    )
    return alert


def tags_manager_view(request):
    alert = ""

    if request.method == 'POST':
        data = request.POST.dict()
        data.pop("csrfmiddlewaretoken")
        Tag.objects.all().update(
            selected=False,
            concept_selected=False,
            procedure_selected=False,
            culturegenerale_selected=False,
            red_selected=False
        )
        for key, value in data.items():
            tag = key.rsplit('.', maxsplit=1)
            if tag[1] == 'selected':
                Tag.objects.filter(name=tag[0]).update(selected=True)
            elif tag[1] == 'concept_selected':
                Tag.objects.filter(name=tag[0]).update(concept_selected=True)
            elif tag[1] == 'procedure_selected':
                Tag.objects.filter(name=tag[0]).update(procedure_selected=True)
            elif tag[1] == 'culturegenerale_selected':
                Tag.objects.filter(name=tag[0]).update(culturegenerale_selected=True)
            elif tag[1] == 'red_selected':
                Tag.objects.filter(name=tag[0]).update(red_selected=True)
        alert = synchronize_cards()

    elif request.method == 'GET':
        Tag.update_tags_info()

    tags = Tag.objects.all()
    context = {
        'tags' : tags,
        'alert' : alert,
    }

    return render(request, 'anky_cards_manager/tags_manager.html', context)

